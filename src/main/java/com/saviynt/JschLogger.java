package com.saviynt;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;

//import java.util.logging.Logger;
import org.apache.logging.log4j.Logger;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

public class JschLogger implements com.jcraft.jsch.Logger{

    //Logger logger = Logger.getLogger(SaviyntJschLogger.class.getName());
    private final Logger logger = LogManager.getLogger(JschLogger.class);

    private final Map<Integer, Level> levelMap = new HashMap(){{
        put(DEBUG, Level.DEBUG);
        put(INFO, Level.INFO);
        put(WARN, Level.WARN);
        put(ERROR, Level.ERROR);
        put(FATAL, Level.FATAL);
    }};

    @Override
    public boolean isEnabled(int level) {
        //logger.isLoggable()
        return logger.isEnabled(levelMap.getOrDefault(level, Level.OFF));
    }

    @Override
    public void log(int level, String message) {
        logger.log(levelMap.getOrDefault(level, Level.OFF), message);
    }

    @Override
    public void log(int level, String message, Throwable cause) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        cause.printStackTrace(pw);
        logger.log(levelMap.getOrDefault(level, Level.OFF), message + " : " + sw.toString());
    }
}
