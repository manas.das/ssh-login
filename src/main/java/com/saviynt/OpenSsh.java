package com.saviynt;

import com.jcraft.jsch.*;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import static com.saviynt.OpenSsh.OpenSshParam.DEFAULT_PORT;

public class OpenSsh {

    private final Logger logger = LogManager.getLogger(OpenSsh.class);

    class OpenSshParam {
        final static int DEFAULT_PORT = 22;

        String host;
        int port;
        int timeout;
        String userName;
        String password;
        String passphrase;
        String privateKey;
        String sshKey;
    }




    private OpenSshParam parseArgs(String[] args) {
        OpenSshParam osp = null;

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        String appName = "SSH Login App";

        final Options options = getOptions();

        try {
            // parse the command line arguments
            CommandLine line = parser.parse(options, args);
            if (line.hasOption("h")) {
                formatter.printHelp(appName, options);
                System.exit(0);
            }

            osp = new OpenSshParam();
            osp.host = line.getOptionValue("host");
            if (line.hasOption("port")) {
                try {
                    osp.port = Integer.parseInt(line.getOptionValue("port"));
                } catch (Exception ex) {
                    logger.error("Port parsing error. Setting to default value.", ex);
                    osp.port = DEFAULT_PORT;
                }
            }
            osp.userName = line.getOptionValue("userName");
            osp.password = line.hasOption("password") ? line.getOptionValue("password") : null;
            osp.passphrase = line.hasOption("passphrase") ? line.getOptionValue("passphrase") : null;
            osp.privateKey = line.hasOption("privateKey") ? line.getOptionValue("privateKey") : null;
            osp.sshKey = line.hasOption("sshKey") ? line.getOptionValue("sshKey") : null;
            if (line.hasOption("timeout")) {
                try {
                    osp.timeout = Integer.parseInt(line.getOptionValue("timeout"));
                } catch (Exception ex) {
                    logger.error("Timeout parsing error.", ex);
                }
            }

        } catch (ParseException exp) {
            System.out.println("Unexpected exception:" + exp.getMessage());
            formatter.printHelp(appName, options);
        }
        return osp;
    }

    private Options getOptions() {
        Options options = new Options();
        options.addOption("h", "help", false, "Print this help message");
        options.addRequiredOption("H", "host", true, "Host name");
        options.addOption("p", "port", true, "port number");
        options.addRequiredOption("u", "userName", true, "User Name");
        options.addOption("P", "password", true, "Password");
        options.addOption("r", "passphrase", true, "Private Key Pass Phrase");
        options.addOption("k", "privateKey", true, "Private Key File Location");
        options.addOption("s", "sshKey", true, "SSH Key File Location");
        options.addOption(Option.builder("t")
                .longOpt("timeout")
                .desc("Connection timeout")
                .hasArg()
                .build());
        return options;
    }


    public OpenSshResponse runCommand(String[] args) {
        final OpenSshParam osp = parseArgs(args);
        OpenSshResponse osr = null;
        JSch.setLogger(new JschLogger());
        JSch jsch = new JSch();
        Session session = null;
        ChannelExec channelExec = null;

        try {
            session = jsch.getSession(osp.userName, osp.host, osp.port);

            if (!isBlank(osp.password)) {
                session.setPassword(osp.password);
            }

            if(!isBlank(osp.sshKey)){
                jsch.addIdentity(osp.sshKey, isBlank(osp.passphrase)?null:osp.passphrase);
            }
            if(!isBlank(osp.privateKey)){
                jsch.addIdentity(osp.privateKey, isBlank(osp.passphrase)?null:osp.passphrase);
            }

            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);

            if (osp.timeout > 0) {
                session.connect(osp.timeout);
            } else {
                session.connect();
            }

            channelExec = (ChannelExec) session.openChannel("exec");
            //channelExec.setPty(true);
            channelExec.setCommand("pwd");

            channelExec.setInputStream(null);
            BufferedReader br = new BufferedReader(new InputStreamReader(channelExec.getInputStream()));
            channelExec.setErrStream(System.err, true);
            BufferedReader errBr = new BufferedReader(new InputStreamReader(channelExec.getErrStream()));

            channelExec.connect();

            String line;
            StringBuilder respSb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                respSb.append(line).append("\n");
            }


            StringBuilder errSb = new StringBuilder();
            while ((line = errBr.readLine()) != null) {
                errSb.append(line).append("\n");
            }


            int exitStatus = 0;
            if (channelExec.isClosed())
                exitStatus = channelExec.getExitStatus();

            osr = OpenSshResponse.builder().output(respSb.toString()).error(errSb.toString()).exitStatus(exitStatus).build();

        } catch (JSchException | IOException e) {
            logger.error(e);
        } finally {
            if (channelExec!=null) channelExec.disconnect();
            if (session!=null) session.disconnect();
        }
        logger.info(osr);

        return osr;
    }

    private boolean isBlank(String str){
        return str == null || str.trim().isEmpty();
    }

}
