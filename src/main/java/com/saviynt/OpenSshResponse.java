package com.saviynt;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class OpenSshResponse {
    String output;
    String error;
    int exitStatus;

    @Override
    public String toString() {
        return "OpenSshResponse{" +
                "output='" + output + '\'' +
                ", error='" + error + '\'' +
                ", exitStatus=" + exitStatus +
                '}';
    }
}
