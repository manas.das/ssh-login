# SSH Login Test
SSH Login Test software 
- Supported for UserID/ Password
- Supported for Private Key
- Supported for Private Key with Passphrase

## Software Build
Use `mvn clean package` to package the software. It will build 2 jars one with dependency and the other without
dependency. Use the fat jar which includes all the dependencies.


## Software Help
You can write the log in a directory preferable to you. Set the `CUSTOM_LOGS_PATH` environment variable.
E.g. `export CUSTOM_LOGS_PATH=<log_dir_path>`. It works with relative path as well as absolute path. Make sure the
directory has write permission.

To get the CLI help use `java -jar target/ssh-login-1.0.0-SNAPSHOT-jar-with-dependencies.jar -H` and will look like below
```
-h,--help               Print this help message
-H,--host <arg>         Host name
-k,--privateKey <arg>   Private Key File Location
-p,--port <arg>         port number
-P,--password <arg>     Password
-r,--passphrase <arg>   Private Key Pass Phrase
-s,--sshKey <arg>       SSH Key File Location
-t,--timeout <arg>      Connection timeout
-u,--userName <arg>     User Name
```

### Use cases
- CLI arguments to connect using User ID & Password
```
-H localhost -p <remote_ssh_port> -u saviyntmaster -P <password>
```
- CLI arguments to connect using SSH Key
```
-H localhost -p <remote_ssh_port> -u saviyntmaster -s <private_key_location>
```
- CLI arguments to connect using SSH Key & Passphrase
```
-H localhost -p <remote_ssh_port> -u saviyntmaster -s <private_key_location> -r <passphrase>
```

## OpenSsh Server
The software can be tested in your local machine. If you need to spin up local unix machine for OpenSsh server,
you can spin up Ubuntu in your Docker Desktop. Go to `remote` dir in this workspace and use `docker compose up`.

Edit file `/etc/ssh/ssh_config` as required.
For example
- To enable/disable password authentication
```
PasswordAuthentication yes/no
```
and restart ssh Service using `service ssh restart`

Command to Generate RSA SSH key with a length of 4096 bits
```
ssh-keygen -t rsa -b 4096
```
Copy the public half of the Key Pair to target machine
```
ssh-copy-id -p <remote_ssh_port> -i ~/.ssh/id_rsa.pub <username>@<host_or_ip>
```
Test the SSH login 
```
ssh -p <remote_ssh_port> -i ~/.ssh/id_rsa <username>@<host_or_ip>
```
